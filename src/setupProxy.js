const { createProxyMiddleware } = require('http-proxy-middleware');

module.exports = function(app) {
  app.use(
    '/amrs',
    createProxyMiddleware({
      target: 'http://10.50.80.115:8090',
      changeOrigin: true,
    })
  );
};