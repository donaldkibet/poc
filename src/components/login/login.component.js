import { TextInput, Button, InlineLoading } from 'carbon-components-react';
import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { Authenticate } from './login.resource';
import './login.component.css';
import { useCurrentUser } from '../../context/current-user';
import logo from '../../assests/health-care-hospital-svgrepo-com.svg';

function Login() {
  const history = useHistory();
  const { setCurrentUser, memorizedCurrentUser } = useCurrentUser();
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [invalidPassword, setInvalidPassword] = useState(false);
  const [isSubmitting, setIsSubmitting] = useState(false);

  const handleSubmit = (e) => {
    const ac = new AbortController();
    e.preventDefault();
    setIsSubmitting(true);
    Authenticate(username, password, ac).then(({ authenticated, user }) => {
      setCurrentUser(user);
      authenticated && history.push('/dashboard');
      setInvalidPassword(!authenticated);
      setIsSubmitting(false);
    });
  };

  useEffect(() => {
    if (memorizedCurrentUser) {
      history.push('/dashboard');
    }
  }, [memorizedCurrentUser, history]);

  return (
    <div className="loginWrapper">
      {isSubmitting && <InlineLoading description="Logging in, please wait" style={{ width: '15rem' }} />}
      {!isSubmitting && (
        <form onSubmit={handleSubmit} className="loginForm">
          <img src={logo} className="App-logo" height={180} width={180} alt="logo" />
          <TextInput id="userName" labelText="Username" onChange={(event) => setUsername(event.target.value)} />
          <TextInput.PasswordInput
            id="password"
            labelText="Password"
            onChange={(event) => setPassword(event.target.value)}
          />
          <div className="buttonContainer">
            <Button type="submit" className="loginBtn" kind="primary">
              Login
            </Button>
          </div>
          {invalidPassword && <p style={{ color: 'red' }}>Invalid password or username </p>}
        </form>
      )}
    </div>
  );
}

export default Login;
