import { baseUrl } from '../../constants';

export const Authenticate = (username, password, ac) => {
  const token = window.btoa(`${username}:${password}`);

  return window
    .fetch(`${baseUrl}session`, {
      headers: {
        Authorization: `Basic ${token}`,
      },
      mode: 'cors',
      signal: ac.signal,
    })
    .then((resp) => resp.json())
    .then((data) => data);
};

export const getCurrentSession = (ac) => {
  return window
    .fetch(baseUrl.concat('session'), { signal: ac.signal })
    .then((response) => response.json())
    .then((resp) => {
      const response = { status: resp.status, header: resp.headers, data: resp };
      return response;
    });
};

export const logUserOut = async () => {
  return window
    .fetch(baseUrl.concat('session'), { method: 'DELETE' })
    .then((response) => response.json())
    .then((resp) => {
      const response = { status: resp.status, header: resp.headers, data: resp };
      return response;
    });
};
