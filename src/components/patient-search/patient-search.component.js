import React, { useEffect, useState } from 'react';
import './patient-search.component.css';
import {
  Table,
  TableHead,
  TableRow,
  TableHeader,
  TableBody,
  TableCell,
  Search,
  TableContainer,
  DataTable,
  DataTableSkeleton,
  Pagination,
} from 'carbon-components-react';
import { fetchPatientByNameOrIdentifier } from './patient-search.resource';
import { usePagination } from '../../hooks/usePagination';

const headers = [
  { key: 'identifiers', header: 'Identifiers' },
  { key: 'name', header: 'Patient Name' },
  { key: 'name', header: 'Patient Name' },
  { key: 'gender', header: 'Gender' },
  { key: 'age', header: 'Age' },
];

function PatientSearch() {
  const [patients, setPatients] = useState([]);
  const [searchTerm, setSearchTerm] = useState('');
  const [loading, setLoading] = useState();
  const { results, goTo, currentPage } = usePagination(patients, 5);

  useEffect(() => {
    if (searchTerm && searchTerm.length > 3) {
      setLoading('pending');
      fetchPatientByNameOrIdentifier(searchTerm).then((patients) => {
        setLoading('resolved');
        setPatients(patients);
      });
    } else {
      setLoading('empty');
      setPatients([]);
    }
  }, [searchTerm]);

  const tableRows = results?.map((patient, index) => {
    return {
      id: `${index}`,
      name: patient.person.display,
      age: patient.person.age,
      gender: patient.person.gender === 'M' ? 'Male' : 'Female',
      identifiers: patient.identifiers.map(({ identifier }) => identifier).join(','),
    };
  });

  return (
    <div>
      <div style={{ padding: '1rem', marginTop: '3rem', border: '1px solid lightgrey' }}>
        <Search
          labelText="search"
          className="searchBar"
          placeholder="Search for patient by patient name or identifier(s)"
          onChange={(event) => setSearchTerm(event.target.value)}
        />
        <div>
          {loading === 'pending' && <DataTableSkeleton rowCount={10} />}
          {loading === 'resolved' && (
            <>
              <DataTable rows={tableRows} headers={headers}>
                {({ rows, headers, getHeaderProps, getTableProps }) => (
                  <TableContainer title="Patient Results">
                    <Table {...getTableProps()}>
                      <TableHead>
                        <TableRow>
                          {headers.map((header) => (
                            <TableHeader {...getHeaderProps({ header })}>{header.header}</TableHeader>
                          ))}
                        </TableRow>
                      </TableHead>
                      <TableBody>
                        {rows.map((row, index) => (
                          <TableRow key={index}>
                            {row.cells.map((cell) => (
                              <TableCell key={cell.id}>{cell.value}</TableCell>
                            ))}
                          </TableRow>
                        ))}
                      </TableBody>
                    </Table>
                  </TableContainer>
                )}
              </DataTable>
              <Pagination
                backwardText="Previous page"
                forwardText="Next page"
                itemsPerPageText="Items per page:"
                page={currentPage}
                pageNumberText="Page Number"
                pageSize={10}
                pageSizes={[10, 20, 30, 40, 50]}
                totalItems={patients?.length}
                onChange={(e) => goTo(e.page)}
              />
            </>
          )}
        </div>
      </div>
    </div>
  );
}
export default PatientSearch;
