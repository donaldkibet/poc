import { baseUrl } from '../../constants';

export const fetchPatientByNameOrIdentifier = async (patientName) => {
  return window
    .fetch(`${baseUrl}patient?q=${patientName}&v=custom:(uuid,identifiers:(identifier,display),person)`)
    .then((resp) => resp.json())
    .then(({ results }) => results)
    .catch((error) => console.log('error', error));
};
