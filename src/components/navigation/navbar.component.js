import React from 'react';
import {
  Header,
  HeaderName,
  HeaderGlobalAction,
  HeaderGlobalBar,
} from 'carbon-components-react/lib/components/UIShell';
import Search20 from '@carbon/icons-react/lib/search/20';
import Notification20 from '@carbon/icons-react/lib/notification/20';
import Logout20 from '@carbon/icons-react/lib/logout/20';
import { useCurrentUser } from '../../context/current-user';
import { logUserOut } from '../login/login.resource';
import { useHistory } from 'react-router-dom';
import Add20 from '@carbon/icons-react/lib/add/20';

const Navbar = () => {
  const history = useHistory();
  const { memorizedCurrentUser, setCurrentUser } = useCurrentUser();

  const handleLogOut = async () => {
    setCurrentUser(null);
    await logUserOut();
  };

  return (
    <>
      {memorizedCurrentUser && (
        <Header aria-label="Ampath POC">
          <HeaderName href="#" prefix="POC">
            AMPATH POC
          </HeaderName>
          <HeaderGlobalBar>
            <HeaderGlobalAction
              aria-label="Search"
              onClick={() => {
                history.push('/patient-search');
              }}>
              <Search20 />
            </HeaderGlobalAction>
            <HeaderGlobalAction
              aria-label="Patient Registration"
              onClick={() => {
                history.push('/patient-registration');
              }}>
              <Add20 />
            </HeaderGlobalAction>
            <HeaderGlobalAction aria-label="Notifications" onClick={() => {}}>
              <Notification20 />
            </HeaderGlobalAction>
            <HeaderGlobalAction aria-label="Log Out" onClick={handleLogOut}>
              <Logout20 />
            </HeaderGlobalAction>
          </HeaderGlobalBar>
        </Header>
      )}
    </>
  );
};

export default Navbar;
