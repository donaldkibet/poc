import React from 'react';
import { Route } from 'react-router';
import { BrowserRouter } from 'react-router-dom';
import Login from './components/login/login.component';
import Dashboard from './components/dashboard/dashboard.component';
import PatientSearch from './components/patient-search/patient-search.component';
import PatientRegistration from './components/patient-registration/patient-registration.component';
import { CurrentUserProvider, useCurrentUser } from './context/current-user';
import Navbar from './components/navigation/navbar.component';

function Root() {
  const user = useCurrentUser();
  console.log(user);
  return (
    <BrowserRouter>
      <CurrentUserProvider>
        <Route path="*" component={Navbar} />
        <Route path="/login" component={Login} exact />
        <Route path="/dashboard" component={Dashboard} exact />
        <Route path="/patient-search" component={PatientSearch} exact />
        <Route path="/patient-registration" component={PatientRegistration} exact />
      </CurrentUserProvider>
    </BrowserRouter>
  );
}
export default Root;
