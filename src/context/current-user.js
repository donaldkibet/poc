import { InlineLoading } from 'carbon-components-react';
import React, { useEffect, useMemo, useState } from 'react';
import { getCurrentSession } from '../components/login/login.resource';
import { useHistory } from 'react-router-dom';

const Loading = () => {
  return (
    <div
      style={{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        height: '100vh',
        width: '100vw',
      }}>
      <InlineLoading style={{ width: 'fit-content' }} description="Loading..." />
    </div>
  );
};

const CurrentUserContext = React.createContext(null);

export const CurrentUserProvider = ({ children }) => {
  const history = useHistory();
  const [isLoading, setIsLoading] = useState(true);
  const [currentUser, setCurrentUser] = useState(null);

  useEffect(() => {
    const ac = new AbortController();
    getCurrentSession(ac).then(({ data }) => {
      const { user } = data;
      setCurrentUser(user);
      setIsLoading(false);
    });
  }, []);

  const memorizedCurrentUser = useMemo(() => currentUser, [currentUser]);

  useEffect(() => {
    if (!memorizedCurrentUser) {
      history.push('/login');
    }
  }, [memorizedCurrentUser, history]);

  return (
    <CurrentUserContext.Provider value={{ memorizedCurrentUser, setCurrentUser }}>
      {isLoading ? <Loading /> : children}
    </CurrentUserContext.Provider>
  );
};

export const useCurrentUser = () => {
  return React.useContext(CurrentUserContext);
};
